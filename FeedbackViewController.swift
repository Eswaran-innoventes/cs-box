//
//  FeedbackViewController.swift
//  cs-box
//
//  Created by Innoventes Technologies on 10/02/16.
//  Copyright © 2016 Innoventes Technologies. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SQLite

class FeedbackViewController: UIViewController {

    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var comment: UITextView!
    var store2 : String = ""
    var details : String = ""
    
    //Store Personal Details screen information
    var companyName : String = ""
    var contactPerson : String = ""
    var Department : String = ""
    var mobileNo : String = ""
    var emailAddr : String = ""
    var landline : String = ""
    var productSno : String = ""
    var selectedProduct : String = ""
    var Rating1 : String = ""
    var Comment1 : String = ""
    
    
    var productDetails: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
   
    @IBAction func sliderValueChanged(sender: UISlider) {
        let currentValue = Int(sender.value)
        textField.text = "\(currentValue) "
        
    }
    
    @IBAction func submit(sender: AnyObject) {
        details = store2 + " Product scale value: "+textField.text!+" Customer Feedback: "+comment.text!
        let alert = UIAlertController(title: "Sucess!", message:"\(details)", preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Okay.", style: .Default) { _ in })
        self.presentViewController(alert, animated: true){}
        
        Comment1 = comment.text!
        Rating1 = textField.text!
        let ratingValue = Int(slider.value)
        
        
       
        
        let parameters = [
            "product": selectedProduct,
            "serial_number": productSno,
            "name": contactPerson,
            "company_name": companyName,
            "mobile": mobileNo,
            "email": emailAddr,
            "rating": ratingValue,
            "comment": Comment1,
            "landline": landline,
            "status": "inprogress",
            "department": Department
        ]
        
        
        let headers = [
            "X_REST_USERNAME": "wsuser",
            "X_REST_PASSWORD": "wspass"
        ]
        
        Alamofire.request(.POST, "http://www.innoventestech.in/consul/backoffice/api/warrantyRegistration", parameters: parameters as? [String : AnyObject], headers: headers, encoding: .JSON)
            .responseJSON { response in
                debugPrint(response)
        }
    }
}