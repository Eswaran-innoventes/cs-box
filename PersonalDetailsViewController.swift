//
//  PersonalDetailsViewController.swift
//  cs-box
//
//  Created by Innoventes Technologies on 10/02/16.
//  Copyright © 2016 Innoventes Technologies. All rights reserved.
//

import UIKit

extension String {
    var isEmail: Bool {
        do {
            let regex = try NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}", options: .CaseInsensitive)
            if regex.firstMatchInString(self, options: NSMatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count)) != nil {
                //NSLog("Valid emaild")
                return true;
            }else{
               // NSLog("In Valid emaild")
                return false;
            }
        } catch {
            return false
        }
    }
    
    var isPhoneNumber: Bool {
        do {
            let PhoneRegex = try NSRegularExpression(pattern: "^\\d{3}\\d{3}\\d{4}$", options: .CaseInsensitive)
            if PhoneRegex.firstMatchInString(self, options: NSMatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count)) != nil {
               // NSLog("Valid phoneno")
                return true;
            }else {
                //NSLog("In Valid phoneno")
                return false;
            }
        } catch {
            return false
        }
    }
}

class PersonalDetailsViewController: UIViewController, UITextFieldDelegate  {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var companyName: UITextField!
    @IBOutlet weak var contactPerson: UITextField!
    @IBOutlet weak var department: UITextField!
    @IBOutlet weak var mobileNo: UITextField!
    @IBOutlet weak var emailAddr: UITextField!
    @IBOutlet weak var landline: UITextField!
    var store1 : String = ""

    @IBOutlet weak var buttonPress: UIButton!
    
    //Store Product Details page information
    var productSno : String = ""
    var selectedProduct : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // let che = "test7"
       
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        animateViewMoving(true, moveValue: 100)
    }
    func textFieldDidEndEditing(textField: UITextField) {
        animateViewMoving(false, moveValue: 100)
    }
    
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:NSTimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = CGRectOffset(self.view.frame, 0,  movement)
        UIView.commitAnimations()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if (textField.text!.isEmpty) {
            let alert = UIAlertController(title: "Oops!", message:"Please Enter Text In The Box", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Okay.", style: .Default) { _ in })
            self.presentViewController(alert, animated: true){}
            
        }else{
              NSLog("String %@", textField.text!);
        }
        
        return true
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true);
    }
 
   override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
            if ((emailAddr.text!.isEmail) && (mobileNo.text!.isPhoneNumber)&&(!(contactPerson.text == ""))){
                    if let Feedback = segue.destinationViewController as? FeedbackController{
                       Feedback.store2 = store1 + " E-Mail: "+emailAddr.text!+" Company Name:"+companyName.text!
                        Feedback.productSno = productSno
                        Feedback.selectedProduct = selectedProduct
                        Feedback.companyName = companyName.text!
                        Feedback.contactPerson = contactPerson.text!
                        Feedback.Department = department.text!
                        Feedback.mobileNo = mobileNo.text!
                        Feedback.emailAddr = emailAddr.text!
                        Feedback.landline = landline.text!
                }
            }
            else{
                if(contactPerson.text == ""){
                    let alert = UIAlertController(title: "Oops!", message:"Contact Person field is empty", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "Okay.", style: .Default) { _ in })
                    self.presentViewController(alert, animated: true){}

                }
                else if (!(mobileNo.text!.isPhoneNumber)){
                    let alert = UIAlertController(title: "Oops!", message:"Invalid Phone Number", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "Okay.", style: .Default) { _ in })
                    self.presentViewController(alert, animated: true){}

                }
                else if(!(emailAddr.text!.isEmail)){
                    let alert = UIAlertController(title: "Oops!", message:"Email Field is Invalid", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "Okay.", style: .Default) { _ in })
                    self.presentViewController(alert, animated: true){}

                }
                
    }
    }
    
}
