//
//  ServiceRequestViewController.swift
//  cs-box
//
//  Created by Innoventes Technologies on 19/02/16.
//  Copyright © 2016 Innoventes Technologies. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SQLite
class ServiceRequestViewController: UIViewController,UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var PickYourProductButton: UIButton!
    @IBOutlet weak var contact: UITextField!
    @IBOutlet weak var chooseProblemButton: UIButton!
    @IBOutlet weak var PickProductViewer: UIPickerView!
    @IBOutlet weak var chooseProblemViewer: UIPickerView!
    @IBOutlet weak var pickNewProductViewer: UIPickerView!
    @IBOutlet weak var otherProblemTextField: UITextField!
    @IBOutlet weak var otherProduct: UITextField!
    @IBOutlet weak var pickNewProduct: UIButton!
    @IBOutlet weak var Submit: UIButton!
    
    var selectedProduct : String = ""
    var choosedProblem : String = ""
    var product = [String]()
    var serialNo = ""
    
    var dataSource1 = [String]()
    var dataSource2 = [String]()
    var dataSource3 = [String]()
    var productCategory: [String] = []
    var problemCategory: [String] = []
    var otherProblem: [String] = ["Other Problem"]
    
    //Store productSno and Product
    var pickedProduct : String = ""
    var productType : String = ""
    var productSno : String = ""
    var ShowProductSerial: [String] = []
    var ShowProductType: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
            self.navigationItem.setHidesBackButton(true, animated: false)
        
        let headers = [
            "X_REST_USERNAME": "wsuser",
            "X_REST_PASSWORD": "wspass"
        ]
        //Product type
        Alamofire.request(.GET, "http://www.innoventestech.in/consul/backoffice/api/ProductCategory", headers: headers, encoding: .JSON).responseJSON() { (response) in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                let title = json["data"]["productCategory"];
                for (_, value) in title {
                    let values: AnyObject = value["category"].description
                    self.productCategory.append(values as! String)
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
            }
        }
        
        //Problem Type
        
        Alamofire.request(.GET, "http://www.innoventestech.in/consul/backoffice/api/ProblemType", headers: headers, encoding: .JSON).responseJSON() { (response) in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                let title = json["data"]["problemType"];
                for (_, value) in title {
                    let values: AnyObject = value["problem_name"].description
                    self.problemCategory.append(values as! String)
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
            }
        }
        let (resultSet, err) = SD.executeQuery("SELECT * FROM warranty_details")
        //print(resultSet.last!["product"])
        if err != nil {
        } else {
            for row in resultSet {
                let result = (row["product"]?.asString())! + ":" + (row["serial_number"]?.asString())!
                self.ShowProductSerial.append(result)
            }
        }
        self.pickNewProductViewer.hidden = true
        self.pickNewProduct.hidden = true
        self.PickProductViewer.hidden = true
        self.chooseProblemViewer.hidden = true
        self.otherProduct.hidden = true
        self.otherProblemTextField.hidden = true
        
        PickProductViewer.delegate = self
        chooseProblemViewer.delegate = self
        pickNewProductViewer.delegate = self
        pickNewProductViewer.dataSource = self
        self.product = ["Other"]
        print(ShowProductSerial)
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        animateViewMoving(true, moveValue: 100)
    }
    func textFieldDidEndEditing(textField: UITextField) {
        animateViewMoving(false, moveValue: 100)
    }
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:NSTimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = CGRectOffset(self.view.frame, 0,  movement)
        UIView.commitAnimations()
    }
    func keyboardWillBeHidden(notification: NSNotification) {
        let contentInsets = UIEdgeInsetsZero
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true);
    }

    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if PickProductViewer == pickerView {
            return dataSource1.count
        }
        if chooseProblemViewer == pickerView {
            
            return dataSource2.count
        }
        if pickNewProductViewer == pickerView {
            return dataSource3.count
        }

        return 1
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if PickProductViewer == pickerView {
            return dataSource1[row]
        }
        if chooseProblemViewer == pickerView {
            return dataSource2[row]
        }
        if pickNewProductViewer == pickerView {
            return dataSource3[row]
        }
        return ""
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if PickProductViewer == pickerView {
            PickYourProductButton.titleLabel?.text = dataSource1[row]
            selectedProduct = dataSource1[row]
            PickProductViewer.hidden = true
            if("Other" == "\(dataSource1[row])"){
                self.pickNewProduct.hidden = false
                self.otherProduct.hidden = false
                self.pickNewProductViewer.hidden = false
                selectedProduct = dataSource1[row]
            }
            else{
                let selectedProductArray = selectedProduct.componentsSeparatedByString(":")
                selectedProduct = selectedProductArray[0]
                serialNo = selectedProductArray[1]
            }
        }
        if chooseProblemViewer == pickerView {
            chooseProblemButton.titleLabel?.text = dataSource2[row]
            choosedProblem = dataSource2[row]
            chooseProblemViewer.hidden = true
            if("Other problem" == "\(dataSource2[row])"){
                self.otherProblemTextField.hidden = false
            }
        }
        if pickNewProductViewer == pickerView {
            pickNewProduct.titleLabel?.text = dataSource3[row]
            selectedProduct = dataSource3[row]
            serialNo = otherProduct.text!
            pickNewProductViewer.hidden = true
        }
    }
    
    @IBAction func pickProduct(sender: AnyObject) {
        dataSource1 = product + ShowProductSerial
        self.PickProductViewer.hidden = false
        [self.PickProductViewer.reloadAllComponents()]
    }
    @IBAction func chooseProblem(sender: AnyObject) {
        dataSource2 = problemCategory
        self.chooseProblemViewer.hidden = false
        [self.chooseProblemViewer.reloadAllComponents()];
    }
    @IBAction func pickNewProduct(sender: AnyObject) {
        dataSource3 = productCategory;
        [self.pickNewProductViewer.reloadAllComponents()]
    }
    @IBAction func serviceRequest(sender: AnyObject) {
        serialNo = otherProduct.text! + serialNo
        
        if ((!(selectedProduct.isEmpty)) && (!(choosedProblem.isEmpty))&&(!(serialNo == ""))) {
            
            let mobile = contact.text!
           
            let otherProblemText = otherProblemTextField.text!
            
            let parameters = [
            "product": selectedProduct,
            "serial_number": serialNo,
            "mobile": mobile,
            "problem": choosedProblem,
            "created_time": "2016-02-08 10:49:59",
            "modified_time": "2016-02-08 10:49:59",
            "status": "open",
            "other_problem": otherProblemText
            ]
            
            let headers = [
            "X_REST_USERNAME": "wsuser",
            "X_REST_PASSWORD": "wspass"
            ]
            Alamofire.request(.POST, "http://www.innoventestech.in/consul/backoffice/api/ServiceRequest", parameters: parameters, headers: headers, encoding: .JSON)
            .responseJSON() { response in
                switch response.result {
                case .Success( _):
                    let alert = UIAlertController(title: "Done!!", message:"Successfully Registered..", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "Okay.", style: .Default) { _ in })
                    self.presentViewController(alert, animated: true){}
                  
                case .Failure(let error):
                    print("Request failed with error: \(error)")
                    let alert = UIAlertController(title: "Opps!!", message:"\(error)..", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "Okay.", style: .Default) { _ in })
                    self.presentViewController(alert, animated: true){}
                }
            }
        }
        else {
            if (selectedProduct.isEmpty){
                let alert = UIAlertController(title: "Opps!!", message:"Please select product..", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Okay.", style: .Default) { _ in })
                self.presentViewController(alert, animated: true){}
            
            }
            else if (choosedProblem.isEmpty){
                let alert = UIAlertController(title: "Opps!!", message:"What Kind of problem do you have..", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Okay.", style: .Default) { _ in })
                self.presentViewController(alert, animated: true){}
                
            }
            else if (serialNo.isEmpty){

                    let alert = UIAlertController(title: "Opps!!", message:"Enter Serial no..", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "Okay.", style: .Default) { _ in })
                    self.presentViewController(alert, animated: true){}
            }
        }
    }
}
