//
//  UpdatePersonalDetailsViewController.swift
//  cs-box
//
//  Created by Innoventes Technologies on 09/03/16.
//  Copyright © 2016 Innoventes Technologies. All rights reserved.
//

import UIKit
class UpdatePersonalDetailsViewController: UIViewController {
    
    @IBOutlet weak var UpdateCompanyName: UITextField!
    @IBOutlet weak var UpdateContactPerson: UITextField!
    @IBOutlet weak var UpdateDepartment: UITextField!
    @IBOutlet weak var UpdateEmailAddress: UITextField!
    @IBOutlet weak var UpdateMobileNo: UITextField!
    @IBOutlet weak var UpdateLandline: UITextField!
    var companyName : String = ""
    var contactPerson : String = ""
    var Department : String = ""
    var mobileNo : String = ""
    var emailAddr : String = ""
    var landline : String = ""
    var productSno : String = ""
    var selectedProduct : String = ""
    var Rating1 : String = ""
    var Comment1 : String = ""
    var warranty_registration_id : String = ""
    @IBOutlet weak var Contact: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: false)
        UpdateCompanyName.text = companyName
        UpdateContactPerson.text = contactPerson
        UpdateDepartment.text = Department
        UpdateMobileNo.text = mobileNo
        UpdateEmailAddress.text = emailAddr
        UpdateLandline.text = landline
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        animateViewMoving(true, moveValue: 100)
    }
    func textFieldDidEndEditing(textField: UITextField) {
        animateViewMoving(false, moveValue: 100)
    }
    
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:NSTimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = CGRectOffset(self.view.frame, 0,  movement)
        UIView.commitAnimations()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if (textField.text!.isEmpty) {
            let alert = UIAlertController(title: "Oops!", message:"Please Enter Text In The Box", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Okay.", style: .Default) { _ in })
            self.presentViewController(alert, animated: true){}
             self.view.endEditing(true)
            
        }else{
            NSLog("String %@", textField.text!);
        }
        
        return true
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true);
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if ((UpdateEmailAddress.text!.isEmail) && (UpdateMobileNo.text!.isPhoneNumber)&&(!(UpdateContactPerson.text == ""))){
            if let UpdateFeedbackDetails = segue.destinationViewController as? UpdateFeedbackDetails{
                UpdateFeedbackDetails.productSno = productSno
                UpdateFeedbackDetails.selectedProduct = selectedProduct
                UpdateFeedbackDetails.companyName = UpdateCompanyName.text!
                UpdateFeedbackDetails.contactPerson = UpdateContactPerson.text!
                UpdateFeedbackDetails.Department = UpdateDepartment.text!
                UpdateFeedbackDetails.mobileNo = UpdateMobileNo.text!
                UpdateFeedbackDetails.emailAddr = UpdateEmailAddress.text!
                UpdateFeedbackDetails.landline = UpdateLandline.text!
                UpdateFeedbackDetails.Rating1 = Rating1
                UpdateFeedbackDetails.Comment1 = Comment1
                UpdateFeedbackDetails.warranty_registration_id = warranty_registration_id
            }
        }
        else{
            if(UpdateContactPerson.text == ""){
                let alert = UIAlertController(title: "Oops!", message:"Contact Person field is empty", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Okay.", style: .Default) { _ in })
                self.presentViewController(alert, animated: true){}
                
            }
            else if (!(UpdateMobileNo.text!.isPhoneNumber)){
                let alert = UIAlertController(title: "Oops!", message:"Invalid Phone Number", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Okay.", style: .Default) { _ in })
                self.presentViewController(alert, animated: true){}
                
            }
            else if(!(UpdateEmailAddress.text!.isEmail)){
                let alert = UIAlertController(title: "Oops!", message:"Email Field is Invalid", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Okay.", style: .Default) { _ in })
                self.presentViewController(alert, animated: true){}
                
            }
            
        }
    }

}
