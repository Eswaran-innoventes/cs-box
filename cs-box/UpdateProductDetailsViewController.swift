//
//  UpdateProductDetailsViewController.swift
//  cs-box
//
//  Created by Innoventes Technologies on 08/03/16.
//  Copyright © 2016 Innoventes Technologies. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class UpdateProductDetailsViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate{
    var companyName : String = ""
    var contactPerson : String = ""
    var Department : String = ""
    var mobileNo : String = ""
    var emailAddr : String = ""
    var landline : String = ""
    var productSno : String = ""
    var selectedProduct : String = ""
    var Rating1 : String = ""
    var Comment1 : String = ""
    
    var productCategory1: [String] = []
    var product = [String]()
    var dataSource1 = [String]()
    var ProductSerial: [String] = []
    var warranty_registration_id : String = ""

    
    
    
    @IBOutlet weak var PickYourProductButton: UIButton!
    
    @IBOutlet weak var ProductSerialNo: UITextField!
    @IBOutlet weak var ConformSerialNo: UITextField!
    
    @IBOutlet weak var PickYourProductViewer: UIPickerView!
    
    @IBOutlet weak var NextScreen: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: false)
        _ = SD.executeChange("DELETE FROM warranty_details WHERE serial_number=?", withArgs: [productSno])
        PickYourProductButton.titleLabel?.text = selectedProduct
        ProductSerialNo.text = productSno
        ConformSerialNo.text = productSno
        print(productSno)
        
        let headers = [
            "X_REST_USERNAME": "wsuser",
            "X_REST_PASSWORD": "wspass"
        ]
        
        Alamofire.request(.GET, "http://www.innoventestech.in/consul/backoffice/api/warrantyRegistration", headers: headers, encoding: .JSON).responseJSON() { (response) in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                let title = json["data"]["warrantyRegistration"];
                for (_, value) in title {
                    let values: AnyObject = value["serial_number"].description
                    self.ProductSerial.append(values as! String)
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
            }
        }

        //Product type
        
        Alamofire.request(.GET, "http://www.innoventestech.in/consul/backoffice/api/ProductCategory", headers: headers, encoding: .JSON).responseJSON() { (response) in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                let title = json["data"]["productCategory"];
                for (_, value) in title {
                    let values: AnyObject = value["category"].description
                    self.productCategory1.append(values as! String)
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
            }
        }
        self.PickYourProductViewer.delegate = self

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true);
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dataSource1.count;
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(dataSource1[row])"
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        PickYourProductButton.titleLabel?.text = dataSource1[row]
        selectedProduct = dataSource1[row]
        PickYourProductViewer.hidden = true
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if (textField.text!.isEmpty) {
            
            let alert = UIAlertController(title: "Oops!", message:"Please Enter Text In The Box", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Okay.", style: .Default) { _ in })
            self.presentViewController(alert, animated: true){}
        }
        
        return true
    }


    @IBAction func LoadProduct(sender: AnyObject) {
        dataSource1 = productCategory1;
        self.PickYourProductViewer.hidden = false
        [self.PickYourProductViewer.reloadAllComponents()];
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let UpdatePersonalDetails = segue.destinationViewController as? UpdatePersonalDetailsViewController {
            UpdatePersonalDetails.productSno = ProductSerialNo.text!
            UpdatePersonalDetails.selectedProduct = selectedProduct
            UpdatePersonalDetails.companyName = companyName
            UpdatePersonalDetails.contactPerson = contactPerson
            UpdatePersonalDetails.Department = Department
            UpdatePersonalDetails.mobileNo = mobileNo
            UpdatePersonalDetails.emailAddr = emailAddr
            UpdatePersonalDetails.landline = landline
            UpdatePersonalDetails.Rating1 = Rating1
            UpdatePersonalDetails.Comment1 = Comment1
            UpdatePersonalDetails.warranty_registration_id = warranty_registration_id
            
        }
    }

}
