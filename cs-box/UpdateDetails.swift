//
//  UpdateDetails.swift
//  cs-box
//
//  Created by Innoventes Technologies on 08/03/16.
//  Copyright © 2016 Innoventes Technologies. All rights reserved.
//

import UIKit

class UpdateDetails: UIViewController {
    
    var companyName : String = ""
    var contactPerson : String = ""
    var Department : String = ""
    var mobileNo : String = ""
    var emailAddr : String = ""
    var landline : String = ""
    var productSno : String = ""
    var selectedProduct : String = ""
    var Rating1 : String = ""
    var Comment1 : String = ""
    var warranty_registration_id : String = ""
    var productDetails: [String] = []
    var ProductSerial: [String] = []

    
    
    @IBOutlet weak var HeadLine: UILabel!
    @IBOutlet weak var UpdateDetailsTextView: UITextView!
    
    @IBOutlet weak var UpdateDetails: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: false)
        
        UpdateDetailsTextView.text = "Product: \(selectedProduct)\n" + "Serail No: \(productSno)\n" + "Company Name: \(companyName)\n" + "Contact Person:\(contactPerson)\n" + "Department: \(Department)\n" + "Mobile No: \(mobileNo)\n" + "Email Address: \(emailAddr)\n" + "Landline: \(landline)"
        ProductSerial.append(productSno)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true);
    }

    @IBAction func UpdateDetailsButton(sender: AnyObject) {
        //performSegueWithIdentifier("UpdateProductDetails", sender: self)
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if let UpdateProductDetails = segue.destinationViewController as? UpdateProductDetailsViewController{
            UpdateProductDetails.productSno = productSno
            UpdateProductDetails.selectedProduct = selectedProduct
            UpdateProductDetails.companyName = companyName
            UpdateProductDetails.contactPerson = contactPerson
            UpdateProductDetails.Department = Department
            UpdateProductDetails.mobileNo = mobileNo
            UpdateProductDetails.emailAddr = emailAddr
            UpdateProductDetails.landline = landline
            UpdateProductDetails.Comment1 = Comment1
            UpdateProductDetails.Rating1 = Rating1
            UpdateProductDetails.warranty_registration_id = warranty_registration_id

        }
    }

}
