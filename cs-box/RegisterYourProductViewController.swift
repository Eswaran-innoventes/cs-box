//
//  RegisterYourProductViewController.swift
//  cs-box
//
//  Created by Innoventes Technologies on 10/02/16.
//  Copyright © 2016 Innoventes Technologies. All rights reserved.
//

import UIKit

class RegisterYourProductViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var selectYourProduct: UIPickerView!
    @IBOutlet weak var productSno: UITextField!
    @IBOutlet weak var conformSno: UITextField!
    @IBOutlet weak var PersonalDetailsButton: UIButton!
    @IBOutlet weak var selectButton: UIButton!
    var product = [String]()
    var dataSource = [String]()
    var selectedProduct = ""
    var store : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        product = ["Transformer","Voltage Stabilizer","Inverter","Ac","Solor","Domestic Inverter"]
        
        //Hide picker view
        self.selectYourProduct.delegate = self
        self.selectYourProduct.hidden = true
        self.productSno.delegate = self
        
        self.conformSno.delegate = self
    }
    
    //To move view up with constant of keyboard height.
    
    func textFieldDidBeginEditing(textField: UITextField) {
        animateViewMoving(true, moveValue: 100)
    }
    func textFieldDidEndEditing(textField: UITextField) {
        animateViewMoving(false, moveValue: 100)
    }
    
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:NSTimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = CGRectOffset(self.view.frame, 0,  movement)
        UIView.commitAnimations()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dataSource.count;
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(dataSource[row])"
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectButton.titleLabel?.text = dataSource[row]
        selectedProduct = dataSource[row]
    }

    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if (textField.text!.isEmpty) {
            
            let alert = UIAlertController(title: "Oops!", message:"Please Enter Text In The Box", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Okay.", style: .Default) { _ in })
            self.presentViewController(alert, animated: true){}
        }

        return true
    }
    
    
    @IBAction func showProduct(sender: AnyObject) {
        dataSource = product;
        //Unhide the picker view
        self.selectYourProduct.hidden = false
        [self.selectYourProduct.reloadAllComponents()];
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if ((productSno.text! == conformSno.text!) && (!productSno.text!.isEmpty)){
            store = "Picked product:" + selectedProduct + " Serial no: " + productSno.text!
            if let personalDetails = segue.destinationViewController as? PersonalDetailsViewController{
                personalDetails.store1 = store
            }
        }
        else {
            let alert = UIAlertController(title: "Oops!", message:"Conform Sno is not matched with Product Sno", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Okay.", style: .Default) { _ in })
            self.presentViewController(alert, animated: true){}
        }
    }
    
}
