//
//  UpdateFeedbackDetails.swift
//  cs-box
//
//  Created by Innoventes Technologies on 09/03/16.
//  Copyright © 2016 Innoventes Technologies. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SQLite



class UpdateFeedbackDetails: UIViewController {
    
    var companyName : String = ""
    var contactPerson : String = ""
    var Department : String = ""
    var mobileNo : String = ""
    var emailAddr : String = ""
    var landline : String = ""
    var productSno : String = ""
    var selectedProduct : String = ""
    var Rating1 : String = ""
    var Comment1 : String = ""
    var warranty_registration_id : String = ""

    @IBOutlet weak var SliderValueTextField: UITextField!
    @IBOutlet weak var Slider: UISlider!
    @IBOutlet weak var Comment: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: false)
        let lastInsertedRow = SD.lastInsertedRowID().rowID.value
        print(lastInsertedRow)
        SliderValueTextField.text = Rating1
        Comment.text = Comment1
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true);
    }
    @IBAction func SliderValueChanged(sender: UISlider) {
        let currentValue = Int(sender.value)
        SliderValueTextField.text = "\(currentValue) "
    }

    @IBAction func UpdateDetails(sender: AnyObject) {
        let headers = [
            "X_REST_USERNAME": "wsuser",
            "X_REST_PASSWORD": "wspass"
        ]
        Comment1 = Comment.text!
        Rating1 =  SliderValueTextField.text!
        let ratingValue = Int(Slider.value)
        let parameters = [
            "product": selectedProduct,
            "serial_number": productSno,
            "name": contactPerson,
            "company_name": companyName,
            "mobile": mobileNo,
            "email": emailAddr,
            "rating": ratingValue,
            "comment": Comment1,
            "landline": landline,
            "status": "inprogress",
            "department": Department
        ]
        
        let url: NSURL! = NSURL(string:"http://www.innoventestech.in/consul/backoffice/api/warrantyRegistration/\(warranty_registration_id)")
        
        Alamofire.request(.PUT, url, parameters: parameters as? [String : AnyObject], headers: headers, encoding: .JSON)
            .responseJSON { response in

                switch response.result {
                case .Success(let data):
                    let json = JSON(data)
                    let title = json["data"]["warrantyRegistration"][0]["warranty_registration_id"].description
                    self.warranty_registration_id.appendContentsOf(title)
                    let (resultSet, err) = SD.executeQuery("SELECT * FROM warranty_details")
                    if err != nil {
                    } else {
                        for row in resultSet {
                            if let serial = row["serial_number"]?.asString() {
                                print("The City name is: \(serial)")
                            }
                            if let product = row["product"]?.asString() {
                                print("The population is: \(product)")
                            }
                        }
                    }
                    if let _ = SD.executeChange("INSERT INTO warranty_details(serial_number, product) VALUES (?, ?)", withArgs: [self.productSno, self.selectedProduct]) {
                        

                    } else {

                    }
                   // self.performSegueWithIdentifier("UpdateDetails", sender: sender)
                    
                case .Failure(let error):
                    let alert = UIAlertController(title: "Oops!!", message:"\(error)..", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "Okay.", style: .Default) { _ in })
                    self.presentViewController(alert, animated: true){}
                }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if let UpdatedDetails = segue.destinationViewController as? UpdatedDetailsViewController {
            UpdatedDetails.productSno = productSno
            UpdatedDetails.selectedProduct = selectedProduct
            UpdatedDetails.companyName = companyName
            UpdatedDetails.contactPerson = contactPerson
            UpdatedDetails.Department = Department
            UpdatedDetails.mobileNo = mobileNo
            UpdatedDetails.emailAddr = emailAddr
            UpdatedDetails.landline = landline
            UpdatedDetails.Rating1 = SliderValueTextField.text!
            UpdatedDetails.Comment1 = Comment.text!
            UpdatedDetails.warranty_registration_id = warranty_registration_id
            
        }
    }
}